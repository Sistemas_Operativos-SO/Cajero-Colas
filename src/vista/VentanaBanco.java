package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import logica.Cola;
import java.lang.Thread;

public class VentanaBanco extends JFrame implements ActionListener{
	//Atributos
	JButton jbTurno = new JButton("Agregar usuarios");
	JButton jbSiguiente = new JButton("Abrir cajero");
	JButton jbCajero = new JButton("Cajero");
	JPanel jpNorte = new JPanel();
	JPanel jpCentro = new JPanel();
	JLabel listaRecibos = new JLabel();
	//Objetos 
	Cola listaCola = new Cola();
	PanelUsuario panelUsuario;
	Hilo hilo;
	
	public VentanaBanco() {
		setLayout(new BorderLayout());
		this.setTitle("Cola cajero");
		this.setSize(800,300);
		this.setLocation(100, 100);
		
		//Se agregan los elemtos al panel
		jpNorte.add(jbTurno);
		jpNorte.add(jbSiguiente);
		jpCentro.add(jbCajero);
		
		
		
		//Botones a la escucha
		jbTurno.addActionListener(this);
		jbSiguiente.addActionListener(this);
		jpCentro.add(listaRecibos);
		
		
		panelUsuario = new PanelUsuario();
		
		jpNorte.setBackground(new Color(255, 255, 255));
		panelUsuario.setBackground(new Color(255, 255, 255));
		
		add(panelUsuario, BorderLayout.CENTER);
		add(jpNorte, BorderLayout.NORTH);	
	}
	public void actionPerformed(ActionEvent e) {
		Object jboton = e.getSource();
		if(jboton == jbTurno) {
			panelUsuario.listaUsuario(listaCola, 1);
			listaCola.asignarTurnoCola(crearRecibosAleatorios());
			panelUsuario.repaint();
		}else if(jboton == jbSiguiente){
			panelUsuario.listaUsuario(listaCola, 2);
			hilo = new Hilo(panelUsuario);
			hilo.start();
		}
		
	}
	
	//Se generan recibos aleatorios del 1 - 10
	private int crearRecibosAleatorios() {
		return (int)(Math.random()*10+1);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	//Abstractos--------------------------------------------
	@Override
	public void actionPerformed(ActionEvent e) {
		
		listaRecibos.setText(" ");
		
		Object jboton = e.getSource();
		if (jboton == jbTurno && Integer.parseInt(jtxtRecibos.getText())<=0) {
			JOptionPane.showMessageDialog(null, "La cantidad de recibos no pueden ser menores a cero o cero, por favor"
					+ " ingrese nuevamente el valor","Advertencia",JOptionPane.WARNING_MESSAGE);
			jtxtRecibos.setText(null);
			//listaCola.recorrerLista();
			//listaRecibos.setText(listaCola.getListaRecibos());
			//listaRecibos.setText(listaCola.getListaRecibos());
		}
		else if(jboton == jbTurno && Integer.parseInt(jtxtRecibos.getText())>0) {
			listaCola.asignarTurnoCola(Integer.parseInt(jtxtRecibos.getText()));
			jtxtRecibos.setText(null);
			listaCola.recorrerLista();
			listaRecibos.setText(listaCola.getListaRecibos());
			//listaRecibos.setText(listaCola.getListaRecibos());
			
		}else if(jboton == jbSiguiente) {			
					
			Hilo hilo = new Hilo(jpCentro, listaCola, listaRecibos);
			hilo.start();
			
		}
		//listaCola.recorrerLista();
		
		
		//listaCola.recorrerLista();
		
	}*/
}
