package vista;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import logica.Cola;

public class PanelUsuario extends JPanel {
	
	// Imagenes usuario
	File ruta;
	Image imagenCajero;
	protected Cola listaCola;
	int estado = 0;

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		ruta = new File("src/imagenes/cajero.jpg");

		try {
			imagenCajero = ImageIO.read(ruta);
		} catch (Exception e) {
			// TODO: handle exception
		}
		g.drawImage(imagenCajero, 50, 20, 150, 150, null);
		if (estado == 1) {
			listaCola.recorrerLista(g);
		} else if (estado == 2) {
			listaCola.salirCola();
			listaCola.recorrerLista(g);
		}

	}

	public void listaUsuario(Cola listaCola, int estado) {
		this.listaCola = listaCola;
		this.estado = estado;
	}
}
