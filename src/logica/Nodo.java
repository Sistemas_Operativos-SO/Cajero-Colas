package logica;


public class Nodo {
	
	//Atributos
	protected int numRecibos;
	protected Nodo siguiente;
	protected int numTurno;
	protected int img;
	protected boolean estado;
	
	//Constructor
	public Nodo(){
		
	}
	
	//getters
	
	public Nodo getNodo() {
		return this.siguiente;
	}
	
	public int getNumeroRecibos() {
		return this.numRecibos;
	}
	
	public int getNumTurno() {
		return this.numTurno;
	}
	
	public int getImg() {
		return this.img;
	}
	
	public boolean getEstado() {
		return this.estado;
	}

}