package logica;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JLabel;

public class Cola<Queue> {

	// Atributos
	public Nodo inicioCola;
	public Nodo finalCola;
	protected String datosCola;
	protected String listaRecibos = "";
	protected Nodo espera;
	protected int turno = 1;
	protected JLabel jlTurno;
	protected JLabel jlNumRecibos;
	protected File ruta;
	protected Image imagenUsu;
	protected int posicionX = 210;
	protected int posicionY = 65;
	protected int img = 1;

	// Constructor----------------------------
	public Cola() {
		// Los nodos apuntan ala vacio
		inicioCola = null;
		finalCola = null;
	}

	// ---------------------------------------
	public boolean estadoCola() {
		// Si la cola esta vacia
		if (inicioCola == null) {
			// Si esta vacia
			return true;
		} else {
			// No este vacia
			return false;
		}
	}
	// InicializarLaCola-------------------------

	public void asignarTurnoCola(int numRecibos) {
		Nodo siguienteCola = new Nodo();
		siguienteCola.numRecibos = numRecibos;
		siguienteCola.numTurno = turno;
		siguienteCola.img = img;
		// No apunta a nadie
		siguienteCola.siguiente = null;

		// ValidaLaFila(true == NoExisteFila)
		if (estadoCola()) {
			inicioCola = siguienteCola;
			finalCola = siguienteCola;
		} else {// (existeCola)
			finalCola.siguiente = siguienteCola;
			finalCola = siguienteCola;
		}
		// System.out.println("Nuevo nodo a la cola");
		turno++;
		img++;
		if (img == 5) {
			img = 1;
		}
	}
	// SacarDeLaCola-----------------------------

	public int salirCola() {
		// estadoDeLaFila(estadoDeLaCola==Fale==HayCola)
		if (!estadoCola()) {
			// AlmacenaLaCantidadDeRecibos
			int numRecibos = inicioCola.numRecibos;
			// ParaSaberSiEstaALInicioDeLaCola
			if (inicioCola == finalCola) {
				if (numRecibos > 5) {
					inicioCola.numRecibos = inicioCola.numRecibos - 5;
				} else {
					inicioCola = null;
					finalCola = null;
				}
			} else {// HayMásPersonaEnLaFila
				if (numRecibos <= 5) {
					inicioCola = inicioCola.siguiente;
					// System.out.println("Sale de la cola");
					// System.out.println("Eliminar nodo");
				} else if (numRecibos > 5) {
					Nodo nuevo_Nodo = inicioCola;
					// nuevo_Nodo.numRecibos = inicioCola.numRecibos - 3;
					inicioCola.numRecibos = inicioCola.numRecibos - 5;
					finalCola.siguiente = nuevo_Nodo;
					finalCola = nuevo_Nodo;
					inicioCola = inicioCola.siguiente;
					nuevo_Nodo.siguiente = null;
					// System.out.println("Noodo a la cola");
				}
			}
			return numRecibos;
		} else {
			// NoHayNadieEnLaaFila
			return 0;
		}
	}

	public void recorrerLista(Graphics g) {
		// System.out.println("Recorre-Lista");
		listaRecibos = "";
		Nodo recorrerLista = inicioCola;
		String colaDatos = "";

		while (recorrerLista != null) {
			asignarImagen(g, recorrerLista.img, recorrerLista.numRecibos, recorrerLista.numTurno);
			recorrerLista = recorrerLista.siguiente;
		}
		posicionX = 210;
		posicionY = 65;
	}

	public void asignarImagen(Graphics g, int img2, int numRecibos, int turnoLista) {
		ruta = new File("src/imagenes/cliente" + img2 + ".png");

		try {
			imagenUsu = ImageIO.read(ruta);
		} catch (Exception e) {
			System.out.print("No encuentra la cola");
		}

		g.drawImage(imagenUsu, posicionX, posicionY, 80, 80, null);
		g.drawString("Turno :" + turnoLista, posicionX, posicionY + 90);
		g.drawString("Recibos :" + numRecibos, posicionX, posicionY + 100);
		posicionX = posicionX + 90;
	}

	// Getter-------------------------------------------
	public String getListaRecibos() {
		return listaRecibos;
	}

}
