package main;


import logica.Cola;
import java.util.Scanner;

import javax.swing.JFrame;

import vista.VentanaBanco; 

public class EjecutaProceso {

	public static void main(String[] args) {
		VentanaBanco jfBanco = new VentanaBanco();
		jfBanco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jfBanco.setVisible(true);
	}
}